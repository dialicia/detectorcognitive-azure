from azure.cognitiveservices.vision.computervision import ComputerVisionClient
from azure.cognitiveservices.vision.computervision.models import OperationStatusCodes
from azure.cognitiveservices.vision.computervision.models import VisualFeatureTypes
from msrest.authentication import CognitiveServicesCredentials

from array import array
import os
from PIL import Image
import sys
import time

from azure.cognitiveservices.vision.face import FaceClient


import requests, uuid, json
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor
import time
import datetime

import os
from tkinter import filedialog
from tkinter import *

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


fl1 = ""
fl2 = ""
fl3 = ""
fl4 = ""
fl5 = ""

def solicitarCarpetas():
    ventana = Tk()
    def carpeta():
        global fl1, fl2, fl3, fl4, fl5

        directorio = filedialog.askdirectory()
        if directorio != "":
            os.chdir(directorio)
        if fl1 == "":
            fl1 = os.getcwd()
        elif fl2 == "":
            fl2 = os.getcwd()
        elif fl3 == "":
            fl3 = os.getcwd()
        elif fl4 == "":
            fl4 = os.getcwd()
        elif fl5 == "":
            fl5 = os.getcwd()
            quit()

    def quit():
        ventana.quit()


    Button(text = "abrir archivo", bg = "pale green", command = carpeta).place(x=10, y=10)
    Button(text = "cerrar archivo", bg="pale green", command=quit).place(x=10, y=40)
    ventana.mainloop()

solicitarCarpetas()


def detectPlaces(images_folder):
    infoLugares = open('infoLugares.txt', "w")
    start = time.time()
    descripciones = 0
    seguro = 0
    # Places and Object
    subscription_key = "add yor key"
    endpoint = "add your endpoint"
    computervision_client = ComputerVisionClient(endpoint, CognitiveServicesCredentials(subscription_key))

    '''
    Describe an Image - local
    This example describes the contents of an image with the confidence score.
    '''
    contenido = os.listdir(images_folder)
    for x in contenido:
        local_image_path = os.path.join(images_folder, x)
        print("===== Describe an Image - local =====")
        # Open local image file
        local_image = open(local_image_path, "rb")
        # Call API
        description_result = computervision_client.describe_image_in_stream(local_image)

        # Get the captions (descriptions) from the response, with confidence level

        print("Description of local image: ")
        if (len(description_result.captions) == 0):
            print("No description detected.")
        else:
            for caption in description_result.captions:
                print("'{}' with confidence {:.2f}%".format(caption.text, caption.confidence * 100))
                descripciones+=1
                seguro+=caption.confidence
    print()
    '''
    END - Describe an Image - local
    '''
    tiempoFinal = (time.time() - start)
    dura = round(tiempoFinal)  # Obtiene solo la parte entera
    promedio = round((seguro / descripciones)*100,2)
    infoLugares.write(str(dura) + '\n')  # cantidad de segundos totales del video procesado.
    infoLugares.write(str(descripciones) + '\n')
    infoLugares.write(str(promedio) + '\n')
    infoLugares.close()




def detectFaces(images_folder):
    start = time.time()
    infoFaces = open('infoFaces.txt', "w")
    # Places and Object
    subscription_key = "add yor key"
    endpoint = "add your endpoint"
    computervision_client = ComputerVisionClient(endpoint, CognitiveServicesCredentials(subscription_key))
    '''
    Detect Faces - local
    This example detects faces in a local image, gets their gender and age,
    and marks them with a bounding box.
    '''
    contenido = os.listdir(images_folder)
    hombres = 0
    mujeres = 0
    for x in contenido:
        local_image_path = os.path.join (images_folder, x)
        print("===== Detect Faces - local =====")
        # Open local image
        local_image = open(local_image_path, "rb")
        # Select visual features(s) you want
        local_image_features = ["faces"]
        # Call API with local image and features
        detect_faces_results_local = computervision_client.analyze_image_in_stream(local_image, local_image_features)

        # Print results with confidence score
        print("Faces in the local image: ")
        if (len(detect_faces_results_local.faces) == 0):
            print("No faces detected.")
        else:
            for face in detect_faces_results_local.faces:
                print(face.gender,"---------------------")
                if face.gender == 'Male':
                    hombres += 1
                elif face.gender == 'Female':
                    mujeres += 1

                print("'{}' of age {} at location {}, {}, {}, {}".format(face.gender, face.age, \
                face.face_rectangle.left, face.face_rectangle.top, \
                face.face_rectangle.left + face.face_rectangle.width, \
                face.face_rectangle.top + face.face_rectangle.height))

        print()
        '''
        END - Detect Faces - local
        '''
    tiempoFinal = (time.time() - start)
    dura = round(tiempoFinal)  # Obtiene solo la parte entera

    infoFaces.write(str(dura) + '\n')  # cantidad de segundos totales del video procesado.
    infoFaces.write(str(hombres) + '\n')
    infoFaces.write(str(mujeres) + '\n')
    infoFaces.close()

def detectAdult(images_folder):
    start = time.time()
    infoAdult = open('infoAdult.txt', "w")
    # Places, Object and Adult
    subscription_key = "add yor key"
    endpoint = "add your endpoint"
    computervision_client = ComputerVisionClient(endpoint, CognitiveServicesCredentials(subscription_key))

    '''
    Detect Adult or Racy Content - local
    This example detects adult or racy content in a local image, then prints the adult/racy score.
    The score is ranged 0.0 - 1.0 with smaller numbers indicating negative results.
    '''
    contenido = os.listdir(images_folder)
    adulto = 0
    racy = 0
    for x in contenido:
        local_image_path = os.path.join (images_folder, x)
        print("===== Detect Adult or Racy Content - local =====")
        # Open local file
        local_image = open(local_image_path, "rb")
        # Select visual features you want
        local_image_features = ["adult"]

        # Call API with local image and features
        detect_adult_results_local = computervision_client.analyze_image_in_stream(local_image, local_image_features)


        # Print results with adult/racy score
        if detect_adult_results_local.adult.is_adult_content == True:
            adulto+=1
        elif detect_adult_results_local.adult.is_racy_content == True:
            racy+=1
        print("Analyzing local image for adult or racy content ... ")
        print("Is adult content: {} with confidence {:.2f}".format(detect_adult_results_local.adult.is_adult_content,
                                                                   detect_adult_results_local.adult.adult_score * 100))
        print("Has racy content: {} with confidence {:.2f}".format(detect_adult_results_local.adult.is_racy_content,
                                                                   detect_adult_results_local.adult.racy_score * 100))
        print()
        '''
        END - Detect Adult or Racy Content - local
        '''
    tiempoFinal = (time.time() - start)
    dura = round(tiempoFinal)  # Obtiene solo la parte entera

    infoAdult.write(str(dura) + '\n')  # cantidad de segundos totales del video procesado.
    infoAdult.write(str(adulto) + '\n')
    infoAdult.write(str(racy) + '\n')
    infoAdult.close()

def detectEmotes(images_folder):
    start = time.time()

    infoEmotes = open('infoEmotes.txt', "w")
    totalHappy = 0
    totalNeutral = 0
    totalAnger = 0
    totalSad = 0

    contador = 1
    subscription_key = "add yor key"
    endpoint = "add your endpoint"
    face_client = FaceClient(endpoint, CognitiveServicesCredentials(subscription_key))

    contenido = os.listdir(images_folder)
    for x in contenido:
        local_image_path = os.path.join(images_folder, x)
        local_image = open(local_image_path, "rb")
        response_detection = face_client.face.detect_with_stream(
            image=local_image,
            detection_model='detection_01',
            recognition_model='recognition_04',
            return_face_attributes=['age', 'emotion'],
        )
        print(x)
        for face in response_detection:
            emotion = face.face_attributes.emotion
            neutral = '{0: 0f}%'.format(emotion.neutral * 100)
            happiness = '{0: 0f}%'.format(emotion.happiness * 100)
            sadness = '{0: 0f}%'.format(emotion.sadness * 100)
            anger = '{0: 0f}%'.format(emotion.anger * 100)
            print("Cara numero: ",contador)
            print("Happy", happiness ," Sad", sadness," Anger", anger, " Neutral", neutral)
            totalHappy = totalHappy + emotion.happiness
            totalSad = totalSad + emotion.sadness
            totalAnger = totalAnger + emotion.anger
            totalNeutral = totalNeutral + emotion.neutral

            contador += 1
        contador = 1
    print("------------------------ Promedio --------------------------------")
    happy = round((totalHappy / len(contenido))*100,2)
    sad = round((totalSad / len(contenido))*100,2)
    anger = round((totalAnger / len(contenido))*100,2)
    neutral = round((totalNeutral / len(contenido)) * 100,2)

    tiempoFinal = (time.time() - start)
    dura = round(tiempoFinal)  # Obtiene solo la parte entera

    infoEmotes.write(str(dura) + '\n')  # cantidad de segundos totales del video procesado.
    infoEmotes.write(str(happy) + '\n')
    infoEmotes.write(str(sad) + '\n')
    infoEmotes.write(str(anger) + '\n')
    infoEmotes.write(str(neutral) + '\n')
    infoEmotes.close()

def translateText(images_folder):
    # --------------------Los siguientes datos son requeridos para hacer uso del traductor----------------------
    infoTrasnlate = open('infoTrasnlate.txt', "w")
    # Añadir los datos de la cuenta azure
    subscription_key = "add yor key"
    endpoint = "add your endpoint"

    # Añadir la Ubicación
    location = "southcentralus"

    start = time.time()

    path = '/translate'
    #constructed_url = endpoint + path
    params = {
        'api-version': '3.0',
        'from': 'es',
        'to': ['en']
    }
    constructed_url = endpoint + path

    headers = {
        'Ocp-Apim-Subscription-Key': subscription_key,
        'Ocp-Apim-Subscription-Region': location,
        'Content-type': 'application/json',
        'X-ClientTraceId': str(uuid.uuid4())
    }

    contenido = os.listdir(images_folder)
    for x in contenido:
        # You can pass more than one object in body.
        local_image_path = os.path.join(images_folder, x)
        local_image = open(local_image_path)

        data= local_image.read()

        #constructor tipo json vacio y se le añade seguidamente los datos del txt
        body = [{}]
        body[0].setdefault('text',data)
        local_image.close()

        #se genera la respuesta traducida al idioma indicado inicialemente
        request = requests.post(constructed_url, params=params, headers=headers, json=body)
        response = request.json()

        #print(json.dumps(response, sort_keys=True, ensure_ascii=False, indent=4, separators=(',', ': ')))

        #Imprime el texto del json
        traducido = response[0]['translations'][0]['text']
        print(traducido)

    tiempoFinal=(time.time()-start)
    dura = round(tiempoFinal,2)  # Obtiene solo la parte entera


    print("Tiempo: " + str(dura))

    infoTrasnlate.write(str(dura) + '\n')
    infoTrasnlate.write(str(len(contenido)) + '\n')
    infoTrasnlate.close()

if __name__ == '__main__':
    with ThreadPoolExecutor(max_workers = 1) as executor:
        executor.submit(detectPlaces, fl1)
        executor.submit(detectFaces, fl2)
        executor.submit(detectAdult, fl3)
        executor.submit(detectEmotes, fl4)
        executor.submit(translateText, fl5)
#------------------------------------------------------INICIO DE GRAFICAS--------------------------------------------------
    places = open('D:\pythonProject\infoLugares.txt','r')
    duraPlaces = 0
    conDescripcion = 0
    promedioPlaces = 0
    cont = 1
    for line in places:
        if cont == 1:
            duraPlaces = int(line)
            cont = cont + 1
        elif cont == 2:
            conDescripcion = int(line)
            cont = cont + 1
        elif cont == 3:
            promedioPlaces = float(line)


    plt.clf()
    fig = plt.figure(u'Analisis General de Escenas')  # Figure
    ax = fig.add_subplot(111)  # Axes coordenada dentro de la ventana.

    nombres = ['Duración', 'Imagenes Analizadas', 'Promedio de acierto']
    datos = [duraPlaces, conDescripcion, promedioPlaces]
    xx = range(len(datos))
    colores = ["blue", "red", "green"]
    ax.bar(xx, datos, color=colores, width=0.5, align='center')
    ax.set_xticks(xx)
    ax.set_xticklabels(nombres)
    ax.grid(axis='y', color='gray', linestyle='dashed')
    plt.title("Analisis General de Escenas")
    plt.savefig('D:\pythonProject\graficas\AnalicisEscenas.png')

#--------------------------------------------------------------------------------------------------------
    faces = open('D:\pythonProject\infoFaces.txt', 'r')
    duraFaces = 0
    conHombre = 0
    conMujer = 0
    cont = 1
    for line in faces:
        if cont == 1:
            duraFaces = int(line)
            cont = cont + 1
        elif cont == 2:
            conHombre = int(line)
            cont = cont + 1
        elif cont == 3:
            conMujer = float(line)

    plt.clf()
    fig = plt.figure(u'Analisis General de Genero')  # Figure
    ax = fig.add_subplot(111)  # Axes coordenada dentro de la ventana.

    nombres = ['Duración', 'Cantidad Hombre', 'Cantidad Mujeres']
    datos = [duraFaces, conHombre, conMujer]
    xx = range(len(datos))
    colores = ["blue", "red", "green"]
    ax.bar(xx, datos, color=colores, width=0.5, align='center')
    ax.set_xticks(xx)
    ax.set_xticklabels(nombres)
    ax.grid(axis='y', color='gray', linestyle='dashed')
    plt.title("Analisis General de Genero")
    plt.savefig('D:\pythonProject\graficas\AnalicisGenero.png')

    adult = open('D:\pythonProject\infoAdult.txt', 'r')
    duraAdult = 0
    conAdult = 0
    conRacy = 0
    cont = 1
    for line in adult:
        if cont == 1:
            duraAdult = int(line)
            cont = cont + 1
        elif cont == 2:
            conAdult = int(line)
            cont = cont + 1
        elif cont == 3:
            conRacy = float(line)

    plt.clf()
    fig = plt.figure(u'Analisis General de Contenido para Adultos')  # Figure
    ax = fig.add_subplot(111)  # Axes coordenada dentro de la ventana.

    nombres = ['Duración', 'Cantidad Hombre', 'Cantidad Mujeres']
    datos = [duraAdult, conAdult, conRacy]
    xx = range(len(datos))
    colores = ["blue", "red", "green"]
    ax.bar(xx, datos, color=colores, width=0.5, align='center')
    ax.set_xticks(xx)
    ax.set_xticklabels(nombres)
    ax.grid(axis='y', color='gray', linestyle='dashed')
    plt.title("Analisis General de Genero")
    plt.savefig('D:\pythonProject\graficas\AnalicisContenidoAdult.png')

#--------------------------------------------------------------------------------------------------------
    emote = open('D:\pythonProject\infoEmotes.txt', 'r')
    duraEmote = 0
    happy = 0
    sad = 0
    anger = 0
    neutral = 0
    cont = 1
    for line in emote:
        if cont == 1:
            duraEmote = int(line)
            cont = cont + 1
        elif cont == 2:
            happy = float(line)
            cont = cont + 1
        elif cont == 3:
            cont = cont+1
            sad= float(line)
        elif cont == 4:
            anger = float(line)
            cont = cont +1
        elif cont == 5:
            neutral = float(line)


    print("duraAdult", duraAdult)

    plt.clf()
    fig = plt.figure(u'Analisis General de Expresiones')  # Figure
    ax = fig.add_subplot(111)  # Axes coordenada dentro de la ventana.

    nombres = ['Duración', 'Feliz', 'Triste','Enojado','Neutral']
    datos = [duraEmote, happy, sad, anger, neutral]
    xx = range(len(datos))
    colores = ["blue", "red", "green", "pink", "yellow"]
    ax.bar(xx, datos, color=colores, width=0.5,  align='center')
    ax.set_xticks(xx)
    ax.set_xticklabels(nombres)
    ax.grid(axis='y', color='gray', linestyle='dashed')
    plt.title("Analisis General de Expresiones")
    plt.savefig('D:\pythonProject\graficas\AnalicisExpresiones.png')

#--------------------------------------------------------------------------------------------------------
    texto = open('D:\pythonProject\infoTrasnlate.txt', 'r')
    duraT = 0
    conT = 0
    cont = 1
    for line in texto:
        if cont == 1:
            duraT = float(line)
            cont = cont + 1
        elif cont == 2:
            conT = int(line)

    plt.clf()
    fig = plt.figure(u'Analisis General de Textes Traducidos')  # Figure
    ax = fig.add_subplot(111)  # Axes coordenada dentro de la ventana.

    nombres = ['Duración', 'Documentos Traducidos']
    datos = [duraT, conT]
    xx = range(len(datos))
    colores = ["blue", "red"]
    ax.bar(xx, datos, color=colores, width=0.5, align='center')
    ax.set_xticks(xx)
    ax.set_xticklabels(nombres)
    ax.grid(axis='y', color='gray', linestyle='dashed')
    plt.title("Analisis General de Textes Traducidos")
    plt.savefig('D:\pythonProject\graficas\AnalisisTextos.png')
