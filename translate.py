import requests, uuid, json
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor
import time
import datetime

# --------------------Los siguientes datos son requeridos para hacer uso del traductor----------------------

# Añadir los datos de la cuenta azure
subscription_key = "add your key" #se coloca la clave
endpoint = "add your endpoint"

# Añadir la Ubicación
location = "southcentralus"
def metodo():
    tiempoFinal = 0
    start = time.time()

    path = '/translate'
    #constructed_url = endpoint + path
    params = {
        'api-version': '3.0',
        'from': 'es',
        'to': ['en']
    }
    constructed_url = endpoint + path

    headers = {
        'Ocp-Apim-Subscription-Key': subscription_key,
        'Ocp-Apim-Subscription-Region': location,
        'Content-type': 'application/json',
        'X-ClientTraceId': str(uuid.uuid4())
    }

    # You can pass more than one object in body.
    file= open('lectura.txt')
    data= file.read()

    #constructor tipo json vacio y se le añade seguidamente los datos del txt
    body = [{}]
    body[0].setdefault('text',data)
    file.close()

    #se genera la respuesta traducida al idioma indicado inicialemente
    request = requests.post(constructed_url, params=params, headers=headers, json=body)
    response = request.json()

    #print(json.dumps(response, sort_keys=True, ensure_ascii=False, indent=4, separators=(',', ': ')))

    #Imprime el texto del json
    traducido = response[0]['translations'][0]['text']
    print(traducido)

    tiempoFinal=(time.time()-start)
    dura = round(tiempoFinal)  # Obtiene solo la parte entera

    print(round(tiempoFinal,2))
    print("Tiempo: " + str(datetime.timedelta(seconds=dura)))

if __name__ == '__main__':
    with ProcessPoolExecutor(max_workers=1) as executor:
        executor.submit(metodo)

