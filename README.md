Proyecto II Sistemas Operativos

# Proyecto de Multiprocesamiento   -  Guía de Uso
Este proyecto fue creado en Python utilizando Azure.
Realizaremos un análisis de archivos de multimedia. En un inicio vamos a considerar películas/vídeos, audios, texto o imágenes. El repositorio
debe estar conformado por múltiples archivos. Implementaremos multiprocesamiento a nivel del repositorio y a cada archivo (multiproceso para cada archivo).


## Instalación
**1. Instalar [Python 3.7.0](https://www.python.org/downloads/release/python-370/) o Superior**

**2. Crear cuenta en [Azure](https://azure.microsoft.com/es-mx/)**

**3. Crear el recurso _cognitiveservice_ que implemente Custom Vision**

<img src="https://user-images.githubusercontent.com/38516078/141401680-79d58e56-465e-402b-8a2d-6283bebb005f.png" width="400">

Y obtener los credenciales correspondientes tales como el **id.Suscripción, extremo y la región**

**4. Módulos:**

- Para el uso del programa se solicitan diferentes módulos entre ellas `azure`, `azure-cognitiveservices`, `pip` y otras que el [IDE Pycharm](https://www.jetbrains.com/es-es/pycharm/download/#section=windows) le indica y le facilita la instalación. 


**5. Un método de instalación común es el `pip install "nombre"`**
 ![image](https://user-images.githubusercontent.com/38516078/141400988-54e3e76d-89d5-458f-a5ac-4dfe81216c65.png)

**6. Para la creación de gráficos con los resultados, se recomienda importar el siguiente módulo:**

```plain
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

```

## Ejecución

**1. Detalles a considerar:**

    + Crear cuenta de Azure
    + Ingresar los credenticiales de una tarjeta de debito o crédito
    + Tomar en consideración las restricciones por ser cuenta gratis
> 

**2. La ejecución funciona de la siguiente manera:**

- Inicialmente se realiza la lectura de la carpeta con diferentes imagenes que representan diferentes escenarios y objetos. Contiene imagenes de personas, sitios y contenido inapropiado.  Con el objetivo de detectar emociones, descripción de escenas, moderación de contenido en imagenes y detectar género. 
- Además se implementa un traductor de texto, este no implementa **Vision** como el anterior pero si es propio de Azure.

**3. Métodos de Ejecución.**

**- Detección de Emociones:**

```plain
def detectEmotes(images_folder):
    start = time.time()

    infoEmotes = open('infoEmotes.txt', "w")
    totalHappy = 0
    totalNeutral = 0
    totalAnger = 0
    totalSad = 0

    contador = 1
    subscription_key = "add your key"
    endpoint = "add you endpoint"
    face_client = FaceClient(ENDPOINT, CognitiveServicesCredentials(API_KEY))

    contenido = os.listdir(images_folder)
    for x in contenido:
        local_image_path = os.path.join(images_folder, x)
        local_image = open(local_image_path, "rb")
        response_detection = face_client.face.detect_with_stream(
            image=local_image,
            detection_model='detection_01',
            recognition_model='recognition_04',
```

**- Detección de Género:**

```plain
def detectFaces(images_folder):
    start = time.time()
    infoFaces = open('infoFaces.txt', "w")
    # Places and Object
    subscription_key = "add your key"
    endpoint = "add you endpoint"
    computervision_client = ComputerVisionClient(endpoint, CognitiveServicesCredentials(subscription_key))
    '''
    Detect Faces - local
    This example detects faces in a local image, gets their gender and age,
    and marks them with a bounding box.
    '''
    contenido = os.listdir(images_folder)
    hombres = 0
    mujeres = 0
    for x in contenido:
```

**- Detección de Objetos, escenas:**

```plain
def detectPlaces(images_folder):
    infoLugares = open('infoLugares.txt', "w")
    start = time.time()
    descripciones = 0
    seguro = 0
    # Places and Object
    subscription_key = "add your key"
    endpoint = "add you endpoint"
    computervision_client = ComputerVisionClient(endpoint, CognitiveServicesCredentials(subscription_key))

    '''
    Describe an Image - local
    This example describes the contents of an image with the confidence score.
    '''
    contenido = os.listdir(images_folder)
    for x in contenido:
        local_image_path = os.path.join(images_folder, x)
        print("===== Describe an Image - local =====")
        # Open local image file
        local_image = open(local_image_path, "rb")
        # Call API
        description_result = computervision_client.describe_image_in_stream(local_image)

        # Get the captions (descriptions) from the response, with confidence level

        print("Description of local image: ")
        if (len(description_result.captions) == 0):
            print("No description detected.")
        else:
            for caption in description_result.captions:
                print("'{}' with confidence {:.2f}%".format(caption.text, caption.confidence * 100))
                descripciones+=1
                seguro+=caption.confidence
    print()
```

**- Detección de contenido inapropiado:**

```plain
def detectAdult(images_folder):
    start = time.time()
    infoAdult = open('infoAdult.txt', "w")
    # Places, Object and Adult
    subscription_key = "add your key"
    endpoint = "add you endpoint"
    computervision_client = ComputerVisionClient(endpoint, CognitiveServicesCredentials(subscription_key))

    '''
    Detect Adult or Racy Content - local
    This example detects adult or racy content in a local image, then prints the adult/racy score.
    The score is ranged 0.0 - 1.0 with smaller numbers indicating negative results.
    '''
    contenido = os.listdir(images_folder)
    adulto = 0
    racy = 0
    for x in contenido:
```

**- Traductor de Texto:**

```plain
def translateText(images_folder):
    # --------------------Los siguientes datos son requeridos para hacer uso del traductor----------------------
    infoTrasnlate = open('infoTrasnlate.txt', "w")
    # Añadir los datos de la cuenta azure
    subscription_key = "add your key"
    endpoint = "add you endpoint"

    # Añadir la Ubicación
    location = "southcentralus"

    start = time.time()

    path = '/translate'
    #constructed_url = endpoint + path
    params = {
        'api-version': '3.0',
        'from': 'es',
        'to': ['en']
```

## Pruebas
Los primeros 4 métodos estan orientados al uso de **Custom Vision** de Azure Cognitive Services y sucede lo siguiente:

La ejecución del programa en Python analizó una carpeta con una colección de imagenes que representan los elementos que se quiere buscar.
Para medir el Multiprocesamiento, es decir, su implementación se puso a prueba la detección de las diferentes métodos mencionados anteriormente utilizado paralelismo, de modo que todos se ejecutan a la vez y se espera que concluya en un tiempo reducido.

Por otro lado, se tiene un método que hace uso del **traductor** de Azure, con este se pretende traducir el contenido de un archivo de texto y que este se muestre en el idioma deseado. 

## Resultados
De las pruebas realizadas a las imagenes ya mencionadas previamente se obtiene algunos resultados.
Estas son las gráficas creadas por el mismo código: 

<img src="https://user-images.githubusercontent.com/38516078/141432396-e8a79a59-0ddc-4b17-9104-becec8aacada.png" width="350">
<img src="https://user-images.githubusercontent.com/38516078/141432410-d3f59642-ac9e-41e3-a448-90b478fdcdcc.png" width="350">

<img src="https://user-images.githubusercontent.com/38516078/141432420-59637c2c-472c-46d4-b04c-bb76bfc0b9fe.png" width="350">
<img src="https://user-images.githubusercontent.com/38516078/141432434-7853f556-572e-409c-9d25-a32978bb6b5d.png" width="350">

<img src="https://user-images.githubusercontent.com/38516078/141432591-3c05a52a-a864-47a0-9c6f-f61db3076148.png" width="350">



## Resultados de Consola - Python
Como bien se muestra en las gráficas anteriores la cantidad de objetos, los tiempos de cada video y los rangos de tiempo, se adjuntan los resultados que se obtuvieron después de la ejecución los elementos en Paralelismo. 
Estos datos son los que se grafican para tener una visión general de los resultados obtenidos.

- Resultados
<img src="https://user-images.githubusercontent.com/38516078/141433543-88bdad0d-ee88-4bf7-a737-486e49138ba1.png" width="350">
<img src="https://user-images.githubusercontent.com/38516078/141433627-33e17680-49e8-425f-9596-c3743b25b017.png" width="300">
<img src="https://user-images.githubusercontent.com/38516078/141433749-4d0d36af-a9e4-43e2-b090-5748b0db6afe.png" width="300">
<img src="https://user-images.githubusercontent.com/38516078/141433850-ca19a4b8-02d0-4855-977e-8008a50ee76a.png" width="300">
<img src="https://user-images.githubusercontent.com/38516078/141434254-3365409e-b434-4e72-b2fe-bb64179f0c52.png" width="300">




